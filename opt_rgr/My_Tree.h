#pragma once
#include"Sorted_tables.h"
#include"Lexer.h"

class My_Tree
{

private:
	struct node_type {
		Lexer::token node_value;
		node_type* parent;
		vector<node_type*> branches;
	};

	node_type* main_parent;
	node_type* current_node;
public:
	My_Tree();
	void add_node_simple_value(string node_name);
	void add_node_lexer_value(Lexer::token smth);
	void go_back();
	void print_all(fstream& file);
	void print(node_type* node, string lines, fstream& file);
};

